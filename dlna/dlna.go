package dlna

import (
	"log"
	"net/url"

	"github.com/huin/goupnp/dcps/av1"
)

const (
	playModeNormal    = "NORMAL"
	playModeRepeatOne = "REPEAT_ONE"
	playModeRepeatAll = "REPEAT_ALL"
)

var (
	renderers = map[string]url.URL{}
	// SelectedRenderer is set to friendly name upon successful setting
	SelectedRenderer   string
	rendererController av1.RenderingControl1
	rendererTransport  av1.AVTransport1
)

// RefreshRenderers re-creates map of renderers
func RefreshRenderers() {
	renderers = make(map[string]url.URL)
	rclients, _, err := av1.NewRenderingControl1Clients()
	if err == nil {
		for _, client := range rclients {
			renderers[client.RootDevice.Device.FriendlyName] = client.RootDevice.URLBase
			log.Printf("RefreshRenderers: found '%s' at '%s'",
				client.RootDevice.Device.FriendlyName, client.RootDevice.URLBase.String())
			if SelectedRenderer == "" {
				SetRenderer(client.RootDevice.Device.FriendlyName)
			}
		}
	}
}

// GetRenderers returns renderers detected using RefreshRenderers()
func GetRenderers() map[string]url.URL {
	return renderers
}

// SetRenderer selects renderer by friendly name
func SetRenderer(friendlyName string) bool {
	if renderer, ok := renderers[friendlyName]; ok {
		log.Printf("Selecting renderer: '%s'", friendlyName)
		ctrl, err := av1.NewRenderingControl1ClientsByURL(&renderer)
		if err == nil {
			SelectedRenderer = friendlyName
			rendererController = *ctrl[0]
		} else {
			log.Printf("Error! Cannot select controller: %s", err)
			return false
		}
		avt, err := av1.NewAVTransport1ClientsByURL(&renderer)
		if err == nil {
			rendererTransport = *avt[0]
		} else {
			log.Printf("Error! Cannot select transport: %s", err)
			return false
		}
		return true
	}
	log.Printf("Error: User selected renderer '%s' is not available. Try Refresh?", friendlyName)
	return false
}

// Play requests upnp player to play a song
func Play(mp3URI string) {
	rendererTransport.SetAVTransportURI(0, mp3URI, "")
	rendererTransport.Play(0, "1")
}

// Stop currently playing track
func Stop() {
	rendererTransport.Stop(0)
}

// IncreaseVolume increases volume by given amount
func IncreaseVolume(amount int) uint16 {
	currentVolume, err := rendererController.GetVolume(0, "Master")
	if err == nil {
		return setVolume(currentVolume + uint16(amount))
	}
	return 0
}

// DecreaseVolume decreases volume by given amount
func DecreaseVolume(amount int) uint16 {
	currentVolume, err := rendererController.GetVolume(0, "Master")
	if err == nil {
		return setVolume(currentVolume - uint16(amount))
	}
	return 0
}

// ToggleRepeat toggles repeat mode (normal, one, all)
func ToggleRepeat() string {
	currentRepeatMode := getRepeat()
	switch currentRepeatMode {
	case playModeNormal:
		return setRepeat(playModeRepeatOne)
	case playModeRepeatOne:
		return setRepeat(playModeRepeatAll)
	default:
		return setRepeat(playModeNormal)
	}
}

func setVolume(volume uint16) uint16 {
	rendererController.SetVolume(0, "Master", volume)
	return volume
}

func setRepeat(mode string) string {
	err := rendererTransport.SetPlayMode(0, mode)
	if err != nil {
		log.Printf("Cannot SetPlayMode: %s", err)
		return playModeNormal
	}
	return mode
}

func getRepeat() string {
	playMode, _, err := rendererTransport.GetTransportSettings(0)
	if err == nil {
		return playMode
	}
	log.Printf("Cannot GetTransportSettings: %s", err)
	return "dunno"
}
