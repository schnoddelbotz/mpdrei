package webserver

import (
	"fmt"
	"net/http"
	"path"
	"strings"

	"github.com/julienschmidt/httprouter"
	"github.com/pierrre/archivefile/zip"
)

func zipHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	if strings.Contains(params.ByName("albumPath"), "..") {
		http.NotFound(w, request)
		return
	}
	albumPath := MP3RootDir + "/" + params.ByName("albumPath")[1:]
	w.Header().Set("Content-Type", "application/zip")
	w.Header().Set("Content-Disposition", fmt.Sprintf(`attachment; filename="%s.zip"`, path.Base(albumPath)))
	zip.Archive(albumPath, w, nil)
}
