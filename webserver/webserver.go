package webserver

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"
	"golang.org/x/image/font/gofont/goregular"

	"github.com/schnoddelbotz/mpdrei/database"
	"github.com/schnoddelbotz/mpdrei/dlna"
)

type assetInfo struct {
	ContentType string
	IsGzipped   bool
}

var (
	assetMap = map[string]assetInfo{
		"index.html":      {"text/html", false},
		"favicon.ico":     {"image/x-icon", false},
		"crossdomain.xml": {"text/xml", false},
		"assets/app.js":   {"application/javascript", true},
		"assets/app.css":  {"text/css", true},
		"spinner.gif":     {"image/gif", false},
	}
	startupTime       = time.Now()
	serverHostName, _ = os.Hostname()
	// Verbose tbd
	Verbose bool
	// MP3RootDir does
	MP3RootDir string
	// ListenAddr defs
	ListenAddr string
	// FilesIndexed nums
	FilesIndexed int
	// AppVersion foobar
	AppVersion string
	serverURI  string
)

// Run starts the webserver
func Run() {
	serverPort := "8080"
	serverIP := "127.0.0.1"
	var serverPortInt = 8080
	var err error

	if !strings.Contains(ListenAddr, ":") {
		log.Fatal("Listen address must be of format [IP]:Port")
	}
	_tmp := strings.Split(ListenAddr, ":")
	serverPort = _tmp[1]
	if serverPortInt, err = strconv.Atoi(serverPort); err != nil {
		log.Fatal("Invalid port for --listen argument")
	}
	if _tmp[0] != "" {
		serverIP = _tmp[0]
	}
	serverURI = fmt.Sprintf("http://%s:%d", serverIP, serverPortInt)
	log.Printf("Webserver ready at http://%s:%d/", serverIP, serverPortInt)

	handler := httprouter.New()

	// built-in asset-based routes (static content)
	handler.GET("/", assetHTTPHandler)
	handler.GET("/index.html", assetHTTPHandler)
	handler.GET("/favicon.ico", assetHTTPHandler)
	handler.GET("/spinner.gif", assetHTTPHandler)
	handler.GET("/assets/*filepath", assetHTTPHandler)
	handler.GET("/goregular.ttf", ttfHandler)
	// db api
	handler.GET("/search/*query", searchHandler)
	handler.GET("/m3u/album/*path", m3uHandler)
	handler.GET("/album/*path", albumHandler)
	handler.GET("/albums/*artistName", albumsHandler)
	handler.GET("/artists", artistsHandler)
	// dlna
	handler.GET("/play/track/:trackID", playTrackHandler)
	handler.GET("/play/album/*path", playListHandler)
	handler.GET("/stop", stopHandler)
	handler.GET("/volumeUp/:amount", volumeUpHandler)
	handler.GET("/volumeDown/:amount", volumeDownHandler)
	handler.GET("/toggleRepeat", toggleRepeatHandler)
	handler.GET("/renderers", renderersHandler)
	handler.GET("/refreshRenderers", refreshRenderersHandler)
	handler.GET("/setRenderer/*renderer", setRendererHandler)
	// cover images/thumbs
	handler.GET("/cover/front/original/*albumPath", frontCoverHandler)
	handler.GET("/cover/front/thumb/*albumPath", frontCoverThumbHandler)
	// aux
	handler.GET("/zip/*albumPath", zipHandler)
	handler.GET("/status", statusHandler)
	// serve MP3s
	handler.ServeFiles("/mp3/*filepath", http.Dir(MP3RootDir))

	// start webserver listener
	err = http.ListenAndServe(ListenAddr, handler)
	if err != nil {
		log.Fatal(err)
	}
}

func assetHTTPHandler(w http.ResponseWriter, request *http.Request, _ httprouter.Params) {
	// serve static / compiled-in assets (using go-bindata)
	requestPath := request.URL.Path[1:]
	responseCode := http.StatusOK
	if requestPath == "" {
		requestPath = "index.html"
	}
	info, ok := assetMap[requestPath]
	if ok {
		gzipExtension := ""
		w.Header().Set("Content-Type", info.ContentType)
		if info.IsGzipped {
			w.Header().Set("Content-Encoding", "gzip")
			gzipExtension = ".gz"
		}
		data, _ := Asset(requestPath + gzipExtension)
		w.Header().Set("Cache-Control", "max-age=86400")
		w.Write(data)
	} else {
		responseCode = 404
		http.NotFound(w, request)
	}

	if Verbose {
		log.Printf("%s %3d %s %s", request.RemoteAddr, responseCode, request.Method, request.URL.Path)
	}
}

func searchHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	query := params.ByName("query")[1:]
	//log.Printf("SEARCH: %s", query)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	result := database.Search(query)
	json, _ := json.Marshal(result)
	w.Write(json)
}

func artistsHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	result := database.GetArtists()
	json, _ := json.Marshal(result)
	w.Write(json)
}

func renderersHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	result := dlna.GetRenderers()
	json, _ := json.Marshal(result)
	w.Write(json)
}

func refreshRenderersHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	dlna.RefreshRenderers()
	result := dlna.GetRenderers()
	json, _ := json.Marshal(result)
	w.Write(json)
}

func setRendererHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	friendlyName := params.ByName("renderer")[1:]
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	response := `{"error":"failed"}`
	if dlna.SetRenderer(friendlyName) {
		response = `{"success":"yes"}`
	}
	w.Write([]byte(response))
}

func playTrackHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	trackID, _ := strconv.Atoi(params.ByName("trackID"))
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	albumPath, trackFileName := database.GetTrackPath(trackID)
	albumPath = url.PathEscape(albumPath)
	albumPath = strings.Replace(albumPath, "%2F", "/", -1) // %2F
	// FIXME: listenAddr --> listen-Port. HostName -> default, override /w flag
	trackURI := fmt.Sprintf("http://%s%s/mp3/%s/%s", serverHostName, ListenAddr,
		albumPath, url.PathEscape(trackFileName))
	dlna.Play(trackURI)
	response := []byte(`{"track":"` + trackURI + `"}`)
	w.Write(response)
}

func stopHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	dlna.Stop()
	w.Write([]byte("{}"))
}

func volumeUpHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	a := params.ByName("amount")
	amount, _ := strconv.Atoi(a)
	newVolume := dlna.IncreaseVolume(amount)
	w.Write([]byte(fmt.Sprintf(`{"volume":%d}`, newVolume)))
}

func volumeDownHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	a := params.ByName("amount")
	amount, _ := strconv.Atoi(a)
	newVolume := dlna.DecreaseVolume(amount)
	w.Write([]byte(fmt.Sprintf(`{"volume":%d}`, newVolume)))
}

func toggleRepeatHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	newMode := dlna.ToggleRepeat()
	w.Write([]byte(fmt.Sprintf(`{"repeat":"%s"}`, newMode)))
}

func playListHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	albumPath := params.ByName("path")[1:]
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	albumURLPath := url.PathEscape(albumPath)
	albumURLPath = strings.Replace(albumURLPath, "%2F", "/", -1)
	m3uURI := fmt.Sprintf("http://%s%s/m3u/album/%s.m3u", serverHostName, ListenAddr, albumURLPath)
	dlna.Play(m3uURI)
	response := []byte(`{"m3u":"` + m3uURI + `"}`)
	w.Write(response)
}

func m3uHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	albumPath := params.ByName("path")[1:]
	albumPath = strings.Trim(albumPath, ".m3u")
	w.Header().Set("Content-Type", "audio/x-mpegurl; charset=utf-8")
	var m3u string
	album := database.GetAlbum(albumPath)
	albumURLPath := url.PathEscape(albumPath)
	albumURLPath = strings.Replace(albumURLPath, "%2F", "/", -1)
	for _, track := range album.Tracks {
		m3u = m3u + fmt.Sprintf("http://%s/mp3/%s/%s\n", request.Host, albumURLPath, url.PathEscape(track.Filename))
	}
	w.Write([]byte(m3u))
}

func albumHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	albumPath := params.ByName("path")[1:]
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	album := database.GetAlbum(albumPath)
	json, _ := json.Marshal(album)
	w.Write(json)
}

func albumsHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	artistName := params.ByName("artistName")
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	albums := database.GetAlbumsByArtist(artistName)
	json, _ := json.Marshal(albums)
	w.Write(json)
}

func ttfHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Cache-Control", "max-age=86400")
	w.Write(goregular.TTF)
}

func statusHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	type Status struct {
		Uptime       string `json:"uptime"`
		Files        int    `json:"files"`
		NumCPU       int    `json:"numcpu"`
		GoVersion    string `json:"goversion"`
		Hostname     string `json:"hostname"`
		HeapMemory   string `json:"heapMemory"`
		AppVersion   string `json:"appversion"`
		DLNARenderer string `json:"upnpserver"`
	}
	type StatusResponse struct {
		Status Status `json:"status"`
	}
	var data Status
	var response StatusResponse
	data.AppVersion = AppVersion
	data.Files = FilesIndexed
	upHours := time.Now().Sub(startupTime).Hours()
	data.Uptime = fmt.Sprintf("%.2f hours (~ %.0f days)", upHours, upHours/24)
	data.NumCPU = runtime.NumCPU()
	data.GoVersion = runtime.Version()
	data.Hostname = serverHostName
	data.DLNARenderer = dlna.SelectedRenderer
	var memstats runtime.MemStats
	runtime.ReadMemStats(&memstats)
	data.HeapMemory = fmt.Sprintf("%.2f MB", float64(memstats.HeapSys)/1024/1024)
	response.Status = data
	myJSON, _ := json.Marshal(response)
	w.Write(myJSON)
}
