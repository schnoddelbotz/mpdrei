package webserver

import (
	"bytes"
	"image/jpeg"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"

	id3 "github.com/casept/id3-go"
	"github.com/julienschmidt/httprouter"
	"github.com/nfnt/resize"
	//id3 "github.com/mikkyang/id3-go"
)

func frontCoverHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	albumPath := params.ByName("albumPath")
	albumPath = MP3RootDir + albumPath
	w.Header().Set("Cache-Control", "max-age=86400")

	// try to find JPGss first
	folderImages := getFolderJPGs(albumPath)
	frontImage := matchFrontImage(folderImages)
	if frontImage != "" {
		imagePath := albumPath + "/" + frontImage
		http.ServeFile(w, request, imagePath)
		return
	}

	// alternatively (no JPGs found), use images contained in MP3s
	image := findFrontCoverMP3(albumPath)
	if len(image) != 0 {
		w.Header().Set("Content-Type", "image/jpeg")
		w.Write(image)
		return
	}

	// fallback: use place-holder image
	data, _ := Asset("mp3.jpg")
	w.Header().Set("Content-Type", "image/jpeg")
	w.Write(data)
}

func frontCoverThumbHandler(w http.ResponseWriter, request *http.Request, params httprouter.Params) {
	albumPath := params.ByName("albumPath")
	albumPath = MP3RootDir + albumPath
	w.Header().Set("Cache-Control", "max-age=86400")

	// try to find JPGss first
	folderImages := getFolderJPGs(albumPath)
	frontImage := matchFrontImage(folderImages)
	if frontImage != "" {
		imagePath := albumPath + "/" + frontImage
		file, err := os.Open(imagePath)
		if err == nil {
			img, err := jpeg.Decode(file)
			if err == nil {
				file.Close()
				m := resize.Resize(45, 0, img, resize.Bicubic)
				jpeg.Encode(w, m, nil)
				return
			}
			log.Printf("Image decode %s error: %s", imagePath, err)
		} else {
			log.Printf("Image open %s error: %s", imagePath, err)
		}
	}

	// alternatively (no JPGs found), use images contained in MP3s
	image := findFrontCoverMP3(albumPath)
	if len(image) != 0 {
		w.Header().Set("Content-Type", "image/jpeg")
		reader := bytes.NewReader(image)
		img, err := jpeg.Decode(reader)
		if err == nil {
			m := resize.Resize(45, 0, img, resize.Bicubic)
			jpeg.Encode(w, m, nil)
			return
		}
		log.Printf("MP3 image decode %s error: %s", albumPath, err)
	}

	// fallback: use place-holder image
	data, _ := Asset("mp3.jpg")
	w.Header().Set("Content-Type", "image/jpeg")
	w.Write(data)
}

func findFrontCoverMP3(searchPath string) []byte {
	var coverImage []byte
	dir, err := os.Open(searchPath)
	if err == nil {
		defer dir.Close()
		filenames, err := dir.Readdirnames(100)
		if err == nil {
			for _, filename := range filenames {
				if strings.HasSuffix(filename, ".mp3") || strings.HasSuffix(filename, ".MP3") {
					mp3, err2 := id3.Open(searchPath + "/" + filename) // CONCURRENCY ISSUE HERE. PANICS
					// test: ab -n 10 -c 1 vs 2 ... -> cover image.
					// https://github.com/mikkyang/id3-go/issues/31
					if err2 == nil {
						defer mp3.Close()
						if trackImage := mp3.Frame("PIC"); trackImage != nil {
							//// FIXME!! See also: APIC. Check Image type! BMP also used.
							if len(trackImage.Bytes()) > 10 {
								return trackImage.Bytes()[6:] // first few bytes are filetype -- fixme...
							}
						}
					}
				}
			}
		}
	}
	return coverImage
}

func getFolderJPGs(searchPath string) []string {
	var images []string
	dir, err := os.Open(searchPath)
	if err == nil {
		defer dir.Close()
		filenames, err := dir.Readdirnames(100)
		if err == nil {
			for _, filename := range filenames {
				if strings.HasSuffix(filename, ".jpg") {
					images = append(images, filename)
				}
			}
		}
	}
	return images
}

func matchFrontImage(images []string) string {
	if len(images) == 0 {
		return ""
	}
	for _, f := range images {
		matched, _ := regexp.MatchString("(?i)(cover.jpg|front|-f.jpg)", f)
		if matched {
			return f
		}
	}
	for _, f := range images {
		matched, _ := regexp.MatchString("(?i)(folder|_a.jpg)", f)
		if matched {
			return f
		}
	}
	return images[0]
}

func matchBackImage(frontImage string, images []string) string {
	if len(images) == 0 {
		return ""
	}
	for _, f := range images {
		matched, _ := regexp.MatchString("(?i)(back|-b.jpg|_b.jpg)", f)
		if matched && f != frontImage {
			return f
		}
	}
	if images[0] != frontImage {
		return images[0]
	}
	return ""
}
