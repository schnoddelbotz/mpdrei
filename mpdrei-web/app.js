
var currentRenderer = '';
var artistList = '';
var localRenderer = 'Local';
var albumTracks = {};

$( document ).ready(function() {
    $("#searchField").focus();
    $("#search").hide();
    $("#album").hide();

    $("#searchField").on('input',
      function(e) { showSearch( $("#searchField").val() ); }
    );

    $("#stop").click(function() {
      stop();
    });

    $("#vol_dn_1").click(function() {
      volumeDown(1);
    });
    $("#vol_up_1").click(function() {
      volumeUp(1);
    });

    $("#repeat").click(function() {
      toggleRepeat();
    });

    $(window).bind( 'hashchange', function(e) {
      hashHasChanged(false);
    });

    var hash = window.location.hash;
    if (hash) {
      hashHasChanged(true);
    } else {
      showIndex();
    }

    $("#fixBottom").fadeOut();

    $.get( "status", function( data ) {
      currentRenderer = data.status.upnpserver;
      $.get( "renderers", function( rdata ) {
        updateRendererList(rdata);
      });
    });

});

function hashHasChanged(initial) {
    var hash = window.location.hash;
    //console.log("HASH CHANGED: "+hash);
    if (hash) {
      if (hash.indexOf('#artist/') > -1) {
        $("#search").hide();
        $("#album").hide();
        $("#content").show();
        showArtist(hash.substring(8));
      } else if (hash.indexOf('#album/') > -1) {
        $("#search").hide();
        $("#content").hide();
        $("#album").show();
        showAlbum(hash.substring(7));
      } else if (hash.indexOf('#status') > -1) {
        $("#search").hide();
        $("#album").hide();
        $("#content").show();
        showStatus();
      } else if (hash.indexOf('#search/') > -1) {
        $("#search").show();
        $("#album").hide();
        $("#content").hide();
        if (initial) {
          var query = hash.substring(8);
          showSearch(query);
          $("#searchField").val(query);
        }
      }
    } else {
      $("#content").show();
      $("#search").hide();
      $("#album").hide();
      $("#searchField").val("");
      $("#searchField").focus();
      showIndex();
    }
}

function showArtist(aName) {
  var artist = decodeURIComponent(aName);
  //console.log("Showing Artist: "+artist);
  $("#content").html("<h2>"+artist+"</h2>");

  $.getJSON( "albums/"+aName, function( data ) {
    var items = [];
    $.each( data.albums, function( key, val ) {
      var coverurl = 'cover/front/thumb/' + val.path;
      var coverimg = '<img class="albumcover_thumb" src="'+coverurl+'">';
      var link = '<a href="#album/'+ val.path +'">' + coverimg +" "+ escapeHtml(val.name) + '</a>';
      items.push( "<li>" + link + "</li>" );
    });
    $( "<ul/>", {
      "class": "my-new-list",
      html: items.join( "" )
    }).appendTo( "#content" );
  });

}

function showAlbum(aPath) {
  var albumPath = decodeURIComponent(aPath);
  albumTracks = {};
  //console.log("Showing Album: "+albumPath);
  $("#album_info").html("");
  $("#album_tracks").html("");

  coverurl = 'cover/front/original/' + aPath;
  coverimg = '<img class="albumcover_large" src="'+coverurl+'">';
  $("#album_info").append(coverimg);
  $("#album_info").append("<br/><br/>");

  var playAlbumLink = $("<button class=\"link\">play album</button>").click(function(){
          playAlbum(aPath);
  });
  $("#album_info").append(playAlbumLink);

  m3ulink = '<button><a href="m3u/album/'+aPath+'.m3u">m3u</a></button>';
  $("#album_info").append(m3ulink);

  ziplink = '<button><a href="zip/'+aPath+'">zip</a></button>';
  $("#album_info").append(ziplink);

  //$("#album_tracks").append("<h3>Tracks</h3>");
  $.getJSON( "album/"+aPath, function( data ) {
    $("#album_head").html("<h2>"+data.name+"</h2>");
    if (data.artist_count == 1) {
      $("#album_head").append("<div>"+'<a href="#artist/'+ data.artist_name +'">' + data.artist_name + '</a>'+"</div>");
    } else {
      $("#album_head").append("<div>Compilation</div>");
    }

    $("#album_tracks").append('<ul class=\"albumtracks\" id="tracks"></ul>');
    $.each( data.tracks, function( key, val ) {
      var trackNum = '';
      if (val.track_no != 0) {
        trackNum = val.track_no + '. ';
      }
      var mp3 = 'mp3/' + albumPath + '/' + val.filename;
      albumTracks[val.id] = mp3;
      var trackText = '<span class="link" onclick="return playTrack('+val.id+',\'\');">&#9654; ' + trackNum + val.artist + " : " + val.name + '</span>';
      if (data.artist_count == 1) {
        trackText = '<span class="link" onclick="return playTrack('+val.id+',\'\');">&#9654; ' + trackNum + val.name + '</span>';
      }
      var track = "<li>"+trackText+"</li>";
      $("#tracks").append(track);
    });
    $("#album_tracks").append('<div><small>path: <a href="mp3/'+albumPath+'">'+albumPath+"</a></small></div>");

  });
}

function showIndex() {
  if (artistList == '') {
    $.get( "artists", function( data ) {
      var artists = '';
      $.each( data.artists, function( key, val ) {
        var link = '<a href="#artist/'+ val.name +'">' + val.name + '</a> | ';
        artists = artists + link;
      });
      artistList = "<small>" + artists + "</small>";
      $("#content").html("<h3>Artists</h3>" + artistList);
    });
  } else {
    $("#content").html("<h3>Artists</h3>" + artistList);
  }
}

function showStatus() {
  // FIXME add static content to index.html...
  $("#content").html("<h2>about</h2>");
  $("#content").append('<p>mpdrei homepage: <a href="https://github.com/schnoddelbotz/mpdrei">https://github.com/schnoddelbotz/mpdrei</a></p>');
  $("#content").append('<h4>go libraries used:</h4>');
  var libs = [
    '<li><a href="https://golang.org/x/image/font/gofont/goregular">golang.org/x/image/font/gofont/goregular</a></li>',
    '<li><a href="https://github.com/huin/goupnp/dcps/av1">github.com/huin/goupnp</a></li>',
    '<li><a href="https://github.com/mikkyang/id3-go">github.com/mikkyang/id3-go</a></li>',
    '<li><a href="https://github.com/julienschmidt/httprouter">github.com/julienschmidt/httprouter</a></li>',
    '<li><a href="https://github.com/jmoiron/sqlx">github.com/jmoiron/sqlx</a></li>',
    '<li><a href="https://github.com/nfnt/resize">github.com/nfnt/resize</a></li>',
    '<li><a href="https://github.com/mattn/go-sqlite3">github.com/mattn/go-sqlite3</a></li>',
    '<li><a href="https://github.com/pierrre/archivefile/zip">github.com/pierrre/archivefile/zip</a></li>',
  ];
  $( "<ul/>", {
      html: libs.join( "" )
  }).appendTo( "#content" );

  $("#content").append('<h4>front-end libraries used:</h4>');
  libs = [
    '<li><a href="http://jquery.com/">jQuery</a></li>',
    '<li><a href="https://getbootstrap.com/">Bootstrap</a></li>',
  ];
  $( "<ul/>", {
      html: libs.join( "" )
  }).appendTo( "#content" );

  $.getJSON( "status", function( data ) {
    $("<h3/>", {html: "server status"}).appendTo( "#content" );
    var items = [];
    $.each( data.status, function( key, val ) {
      items.push( "<li>" + key + " : " + val + "</li>" );
    });
    $( "<ul/>", {
      "class": "my-new-list",
      html: items.join( "" )
    }).appendTo( "#content" );
  });
}

function showSearch(query) {
  if (query=="") {
    window.location.hash = '#';
    showIndex();
    return;
  }
  $.getJSON( "search/"+query, function( data ) {
    window.location.hash = '#search/' + query;
    $("#content").hide();
    $("#s_artists").html("");
    $("#s_albums").html("");
    $("#s_tracks").html("");

    if (data.artists) {
      $("<h2/>", {html: "Artists"}).appendTo( "#s_artists" );
      var items = [];
      $.each( data.artists, function( key, val ) {
        var link = '<a href="#artist/'+ val.name +'">' + val.name + '</a>';
        items.push( "<li>" + link + "</li>" );
      });
      $( "<ul/>", {
        "class": "searchartists",
        html: items.join( "" )
      }).appendTo( "#s_artists" );
    }

    if (data.albums) {
      $("<h2/>", {html: "Albums"}).appendTo( "#s_albums" );
      var items = [];
      $.each( data.albums, function( key, val ) {
        var coverurl = 'cover/front/thumb/' + val.path;
        var coverimg = '<img class="albumcover_thumb" src="'+coverurl+'">';
        var details = '<br/><small>' + '<a href="#artist/'+ val.artist_name +'">' + escapeHtml(val.artist_name) + '</a>' + '</small>';
        if (val.artist_count > 1) {
          details = '<br/><small>Compilation ('+val.artist_count+' artists)</small>';
        }
        var link = '<a href="#album/'+ val.path +'">' + escapeHtml(val.name) + '</a>';
        items.push( '<li><div class="cimg">' + coverimg +'</div><div class="alblink">'+ link + details + '</div></li>');
      });
      $( "<ul/>", {
        "class": "searchalbums",
        html: items.join( "" )
      }).appendTo( "#s_albums" );
    }

    if (data.tracks) {
      $("<h2/>", {html: "Tracks"}).appendTo( "#s_tracks" );
      $("#s_tracks").append('<ul class=\"searchtracks\" id="stracks"></ul>');
      $.each( data.tracks, function( key, val ) {
        var trackText = '&#9654; ' + val.name;
        var mp3 = 'mp3/' + val.albumpath + '/' + val.filename.replace(/'/g, "\\'");
        var link = '<span class="link" onclick="playTrack('+val.id+',\''+mp3+'\');">'+  trackText +'</span>';
        var etc = "<br/><small>" + '<a href="#artist/'+ val.artist +'">' + escapeHtml(val.artist) + '</a>' + " | " + '<a href="#album/'+ val.albumpath +'">' + escapeHtml(val.album) + '</a>' + '</small>';
        $("#stracks").append("<li>" +link+ etc + "</li>");
      });
    }

  });
}

function playTrack(trackId,url) {
  if (currentRenderer == localRenderer) {
    if (url=='') {
      url=albumTracks[trackId];
    }
    console.log('Play '+trackId+' : '+url);
    // replace all event listeners
    var el = document.getElementById('player'),
    elClone = el.cloneNode(true);
    el.parentNode.replaceChild(elClone, el);
    var audio = $("#player");
    $("#mp3").attr("src", url);
    //audio[0].pause();
    audio[0].load();
    audio[0].oncanplaythrough = audio[0].play();
    audio[0].addEventListener("ended", function () {
      playNextAlbumTrackForId(trackId);
    }, false);
    return;
  }
  $("#fixBottom").show();
  $.get( "play/track/"+trackId, function( data ) {
    setTimeout( function(){ $("#fixBottom").fadeOut() },1000);
  });
}

function playNextAlbumTrackForId(id) {
  var keys = [];
  for (var key in albumTracks) {
      keys.push(parseInt(key));
  }
  var current = keys.indexOf(id);
  if (current+1 < keys.length) {
    var nextId = keys[current+1];
    playTrack(nextId,'');
  }
}

function playAlbum(aPath) {
  if (currentRenderer == localRenderer) {
    alert('For local album playback, just click track to start with');
    return;
  }
  $("#fixBottom").show();
  $.get( "play/album/"+aPath, function( data ) {
    setTimeout( function(){ $("#fixBottom").fadeOut() },1000);
  });
}

function stop() {
  if (currentRenderer == localRenderer) {
    // remove all event listeners from #player
    var el = document.getElementById('player'),
    elClone = el.cloneNode(true);
    el.parentNode.replaceChild(elClone, el);
    //$("#player")[0].pause();
    return;
  }
  $("#fixBottom").show();
  $.get( "stop", function( data ) {
    setTimeout( function(){ $("#fixBottom").fadeOut() },1000);
  });
}

function volumeUp(amount) {
  if (currentRenderer == localRenderer) {
    p = document.getElementById("player");
    if (p.volume * 100 < 100) {
      p.volume += 0.1;
      $("#volume").html( (p.volume * 100).toFixed(0));
    }
    return;
  }
  $("#fixBottom").show();
  $.get( "volumeUp/"+amount, function( data ) {
    $("#volume").html(data.volume);
    setTimeout( function(){ $("#fixBottom").fadeOut() },250);
  });
}
function volumeDown(amount) {
  if (currentRenderer == localRenderer) {
    p = document.getElementById("player");
    if (p.volume * 100 > 10) {
      p.volume -= 0.1;
      $("#volume").html( (p.volume * 100).toFixed(0));
    }
    return;
  }
  $("#fixBottom").show();
  $.get( "volumeDown/"+amount, function( data ) {
    $("#volume").html(data.volume);
    setTimeout( function(){ $("#fixBottom").fadeOut() },250);
  });
}

function toggleRepeat() {
  if (currentRenderer == localRenderer) {
    p = document.getElementById("player");
    switch(p.loop) {
      case false:
        $("#repeat").html("&#119047; 1");
        p.loop = true;
        break;
      default:
        $("#repeat").html("&#119047;");
        p.loop = false;
    }
    return;
  }
  $("#fixBottom").show();
  $.get( "toggleRepeat", function( data ) {
    switch(data.repeat) {
    case "REPEAT_ONE":
        $("#repeat").html("&#119047; 1");
        break;
    case "REPEAT_ALL":
        $("#repeat").html("&#119047; all");
        break;
    default:
        $("#repeat").html("&#119047;");
    }
    setTimeout( function(){ $("#fixBottom").fadeOut() },250);
  });
}

function refreshRenderers() {
  $("#fixBottom").show();
  $.get( "refreshRenderers", function( data ) {
    updateRendererList(data);
    setTimeout( function(){ $("#fixBottom").fadeOut() },250);
  });
}

function setRenderer(r) {
  if (r==localRenderer) {
    currentRenderer = localRenderer;
    $.get("renderers", function( rdata ) {
        updateRendererList(rdata);
    });
    return;
  }
  $.get( "setRenderer/"+r.replace(/#/g, '%23'), function( data ) {
    if (data.error) {
      alert("Didn't work, sorry.");
    } else {
      currentRenderer = r;
      $.get( "renderers", function( rdata ) {
        updateRendererList(rdata);
      });
    }
  });
}

function updateRendererList(data) {
  $("#rendererList").html("");
  data[localRenderer] = {'host':'localhost'};
  if (Object.keys(data).length == 1) {
    currentRenderer = localRenderer;
  }
  $.each( data, function( key, val ) {
    var oc = 'onclick="setRenderer(\''+key+'\'); return false;"'
    var friendlyName = key;
    if (currentRenderer == friendlyName) {
      friendlyName = '<b>'+friendlyName+' *</b>';
    }
    var li = '<li><a class="dropdown-item" '+oc+' href="#">'+friendlyName+'</a></li>';
    $("#rendererList").append(li);
  });
}

// thanks to: http://stackoverflow.com/questions/24816/escaping-html-strings-with-jquery
var entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};
function escapeHtml (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
}
