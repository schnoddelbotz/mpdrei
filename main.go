package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	//id3 "github.com/mikkyang/id3-go"
	// https://github.com/casept/id3-go/commit/2e21798b4999ccd0cd10fdb37e427712998d6fc2
	id3 "github.com/casept/id3-go"

	"github.com/schnoddelbotz/mpdrei/database"
	"github.com/schnoddelbotz/mpdrei/dlna"
	"github.com/schnoddelbotz/mpdrei/webserver"
)

var (
	// AppVersion is defined at build time using ldflags
	AppVersion   string
	filesIndexed int
	flagVerbose  = flag.Bool("verbose", false, "verbose output")
	flagRoot     = flag.String("root-path", "", "MP3 root path")
	flagVersion  = flag.Bool("Version", false, "print version")
	listenAddr   = flag.String("listen", ":8080", "let webserver listen on, [IP]:PORT")
	dlnaRenderer = flag.String("dlna-renderer", "", "DLNA renderer device/player, friendly name")
)

func main() {
	webserver.AppVersion = Version()
	flag.Parse()
	if *flagVersion {
		println(Version())
		os.Exit(0)
	}
	if *flagRoot == "" {
		log.Fatalln("You must provide -root-path ... to your MP3 collection")
	}
	// if *dlnaRenderer == "" {
	// 	log.Fatalln("You must provide -dlna-renderer ... to your DLNA renderer (IP:port)")
	// }
	if *flagVerbose {
		webserver.Verbose = true
	}
	log.Printf("mpdrei version %s starting up...", Version())
	database.InitDB()

	go func() {
		log.Println("Indexing media...")
		startupTime := time.Now()
		filepath.Walk(*flagRoot, analyzeMediaFile)
		duration := time.Now().Sub(startupTime).Seconds()
		log.Printf("Indexed %d files in %.2f seconds", filesIndexed, duration)
		webserver.FilesIndexed = filesIndexed
		// fixme: start fs watch go routine here
	}()

	go func() {
		log.Println("Discovering renderers...")
		dlna.RefreshRenderers()
		if *dlnaRenderer != "" {
			dlna.SetRenderer(*dlnaRenderer)
		}
	}()

	webserver.ListenAddr = *listenAddr
	webserver.MP3RootDir = *flagRoot
	webserver.Run()
}

func analyzeMediaFile(fpath string, info os.FileInfo, err error) error {
	extension := filepath.Ext(fpath)
	if extension == ".mp3" || extension == ".MP3" {

		var tag database.ID3tag
		tag.Filepath = strings.TrimPrefix(fpath, *flagRoot+"/")

		mp3, err := id3.Open(fpath)
		if err == nil {
			defer mp3.Close()
			// for _, f := range mp3.AllFrames() {
			//  log.Printf("   i:%s --> f:%s", f.Id(), f.String())
			// }
			if trackNo := mp3.Frame("TRK"); trackNo != nil {
				tNum := strings.Split(trackNo.String(), "/")
				tag.TrackNo, _ = strconv.Atoi(tNum[0])
				//fmt.Printf("USING TRK __%s_=_%d__ for %s !!!!!!!!\n", trackNo.String(), tag.TrackNo, fpath)
			} else if trackNo := mp3.Frame("TRCK"); trackNo != nil {
				tNum := strings.Split(trackNo.String(), "/")
				tag.TrackNo, _ = strconv.Atoi(tNum[0])
				//fmt.Printf("USING TRCK __%s_=_%d__ for %s!!!!!!!!\n", trackNo.String(), tag.TrackNo, fpath)
			} // else {
			//  fmt.Printf("NO TR(C)K for %s\n", fpath)
			// }

			tag.Artist = strings.Trim(mp3.Artist(), " \u0000")
			tag.Album = strings.Trim(mp3.Album(), " \u0000")
			tag.Title = strings.Trim(mp3.Title(), " \u0000")

			// use directory names / filenames if no ID3 found
			if tag.Album == "" {
				tag.Album = path.Base(path.Dir(fpath))
				if *flagVerbose {
					fmt.Printf("Using album '%s' from dirname for %s\n", tag.Album, fpath)
				}
			}
			if tag.Artist == "" {
				tag.Artist = path.Base(path.Dir(path.Dir(fpath)))
				if *flagVerbose {
					fmt.Printf("Using artist '%s' from dirname for %s\n", tag.Artist, fpath)
				}
			}
			if tag.Title == "" {
				tag.Title = strings.Trim(path.Base(fpath), ".mp3")
				if *flagVerbose {
					fmt.Printf("Using title '%s' from filename for %s\n", tag.Title, fpath)
				}
			}
		} else {
			if *flagVerbose {
				fmt.Printf("FAIL ID3, using path infos: %s\n", fpath)
			}
			tag.Album = path.Base(path.Dir(fpath))
			tag.Artist = path.Base(path.Dir(path.Dir(fpath)))
			tag.Title = strings.Trim(path.Base(fpath), ".mp3")
		}
		// will lookup artist/album id or create id if non-existant
		artistID := database.GetArtistID(tag.Artist)
		albumID := database.GetAlbumID(tag.Album, filepath.Dir(tag.Filepath))
		database.CreateTrack(albumID, artistID, tag.Title, filepath.Base(tag.Filepath), tag.TrackNo)
		filesIndexed = filesIndexed + 1
		if filesIndexed%100 == 0 {
			fmt.Printf("Files indexed: %d\r", filesIndexed)
			webserver.FilesIndexed = filesIndexed
		}
	}
	return nil
}

// Version returns current mpdrei version as string
func Version() string {
	if len(AppVersion) == 0 {
		AppVersion = "0.0.0-dev"
	}
	return AppVersion
}
