// Package database acts as storage backend for mpdrei's webserver.
package database

import (
	"log"

	"github.com/jmoiron/sqlx"
	// import SQL driver
	_ "github.com/mattn/go-sqlite3"
)

var db *sqlx.DB

// InitDB inits DB
func InitDB() {
	var err error
	log.Print("Initalizing database ...")
	//DbFile := "mp3.db?cache=shared&mode=rwc&_busy_timeout=9999999"
	DbFile := "file::memory:?mode=memory&cache=shared"
	db, err = sqlx.Open("sqlite3", DbFile)
	if err != nil {
		log.Fatal(err)
	}
	db.SetMaxOpenConns(1)
	_, err = db.Exec(dbSchema)
	if err != nil {
		log.Fatalf("Error initializing DB: %s", err)
	}
	//db.Exec("PRAGMA foreign_keys = ON")
}

// GetArtistID doc
func GetArtistID(name string) int {
	var artists ArtistList
	err := db.Select(&artists, "SELECT * FROM artist WHERE name LIKE ?", name)
	if err != nil {
		log.Printf("ERR: %s", err)
	}
	if len(artists) == 0 {
		return createArtist(name)
	}
	return artists[0].ID
}

// GetAlbumID doc
func GetAlbumID(name string, path string) int {
	var albums AlbumList
	err := db.Select(&albums, "SELECT id FROM album WHERE name=? AND path=?", name, path)
	if err != nil {
		log.Printf("ERRA: %s", err)
	}
	if len(albums) == 0 {
		return createAlbum(name, path)
	}
	//log.Printf("ALBS: %+v", albums)
	return albums[0].ID
}

// GetTrackPath doc
func GetTrackPath(trackID int) (string, string) {
	type Trackpath struct {
		Path     string `json:"path" db:"path"`
		Filename string `json:"name" db:"filename"`
	}
	var paths []Trackpath
	q := `SELECT album.path,track.filename FROM track,album WHERE track.id=? AND track.album_id=album.id`
	db.Select(&paths, q, trackID)
	return paths[0].Path, paths[0].Filename
}

func createAlbum(name string, path string) int {
	q, err := db.Exec("INSERT INTO album (name, path) VALUES (?,?)", name, path)
	if err == nil {
		id, _ := q.LastInsertId()
		return int(id)
	}
	log.Fatalf("Failed to create album %s : %s", name, err)
	return 0
}

func createArtist(name string) int {
	q, err := db.Exec("INSERT INTO artist (name) VALUES (?)", name)
	if err == nil {
		id, _ := q.LastInsertId()
		return int(id)
	}
	log.Fatalf("Failed to create artist %s: %s", name, err)
	return 0
}

// CreateTrack doc
func CreateTrack(albumID int, artistID int, name string, filename string, trackNo int) {
	query := "INSERT INTO track (album_id, artist_id, name, filename, track_no) VALUES (?,?,?,?,?)"
	_, err := db.Exec(query, albumID, artistID, name, filename, trackNo)
	if err != nil {
		log.Fatalf("Error inserting track: %s", err)
	}
}

// GetAlbum gets
func GetAlbum(path string) Album {
	var albums []Album
	var album Album
	var artistMap = map[string]bool{}
	var artist string
	db.Select(&albums, "SELECT * FROM album WHERE path=?", path)
	// QUIRK/Work-Around: ID3 tag "album" may differ inside one directory.
	for _, tmpalbum := range albums {
		var tracks []Track
		db.Select(&tracks, "SELECT track.*, artist.name artist FROM track,artist WHERE album_id=? AND track.artist_id=artist.id ORDER BY track_no", tmpalbum.ID)
		for _, track := range tracks {
			album.Tracks = append(album.Tracks, track)
			artistMap[track.Artist] = true
			artist = track.Artist
		}
	}
	album.ArtistCount = len(artistMap)
	if album.ArtistCount == 1 {
		album.ArtistName = artist
	}
	if len(albums) > 0 {
		album.Name = albums[0].Name
	}
	return album

}

// Search doc
func Search(query string) SearchResult {
	var result SearchResult
	wildcard := "%" + query + "%"
	db.Select(&result.Artists, "SELECT * FROM artist WHERE name LIKE ? ORDER BY name LIMIT 25", wildcard)
	db.Select(&result.Albums, "SELECT album.*,artist.name artist_name,COUNT(DISTINCT artist_id) artist_count "+
		"FROM album,track,artist where artist.id=track.artist_id and album.name LIKE ? "+
		"AND track.album_id=album.id GROUP BY album.id LIMIT 20", wildcard)
	db.Select(&result.Tracks,
		"SELECT track.*, artist.name artist, album.name album, album.path albumpath FROM track,artist,album "+
			"WHERE track.artist_id=artist.id AND track.album_id=album.id "+
			"AND track.name LIKE ? ORDER BY name LIMIT 15", wildcard)
	return result
}

// GetAlbumsByArtist doc
func GetAlbumsByArtist(artistName string) Albums {
	var albums Albums
	artistName = artistName[1:]
	err := db.Select(&albums.Albums, "SELECT DISTINCT album.* FROM album,track,artist "+
		"WHERE track.album_id=album.id AND track.artist_id=artist.id AND "+
		"artist.name=? ORDER BY album.name", artistName)
	if err != nil {
		albums.Error = err.Error()
	}
	return albums
}

// GetArtists doc
func GetArtists() Artists {
	var artists Artists
	err := db.Select(&artists.Artists, "SELECT name FROM artist ORDER BY name")
	if err != nil {
		artists.Error = err.Error()
	}
	return artists
}
