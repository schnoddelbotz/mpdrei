package database

// ID3tag doc
type ID3tag struct {
	Filepath string `json:"path"`
	Artist   string `json:"artist"`
	Title    string `json:"title"`
	Album    string `json:"album"`
	TrackNo  int    `json:"track_no" db:"track_no"`
}

// Artist doc
type Artist struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Artists doc
type Artists struct {
	Error   string   `json:"error"`
	Artists []Artist `json:"artists"`
}

// Album doc
type Album struct {
	ID          int     `json:"id"`
	Name        string  `json:"name" db:"name"`
	Path        string  `json:"path" db:"path"`
	Tracks      []Track `json:"tracks"`
	ArtistName  string  `json:"artist_name" db:"artist_name"`
	ArtistCount int     `json:"artist_count" db:"artist_count"`
}

// Albums doc
type Albums struct {
	Error  string  `json:"error"`
	Albums []Album `json:"albums"`
}

// Track doc
type Track struct {
	ID        int    `json:"id"`
	AlbumID   int    `json:"album_id" db:"album_id"`
	ArtistID  int    `json:"artist_id" db:"artist_id"`
	TrackNo   int    `json:"track_no" db:"track_no"`
	AlbumPath string `json:"albumpath" db:"albumpath"`
	Artist    string `json:"artist"`
	Album     string `json:"album"`
	Name      string `json:"name"`
	Filename  string `json:"filename"`
}

// Tracks doc
type Tracks struct {
	Error  string  `json:"error"`
	Tracks []Track `json:"tracks"`
}

// SearchResult doc
type SearchResult struct {
	Error   string   `json:"error"`
	Artists []Artist `json:"artists"`
	Albums  []Album  `json:"albums"`
	Tracks  []Track  `json:"tracks"`
}

// ArtistList doc
type ArtistList []Artist

// AlbumList doc
type AlbumList []Album

// TrackList doc
type TrackList []Track
