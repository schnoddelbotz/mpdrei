package database

const dbSchema = `
CREATE TABLE artist (
  id        INTEGER     PRIMARY KEY,
  name      VARCHAR(80)
);
CREATE TABLE album (
  id        INTEGER    PRIMARY KEY,
  name      VARCHAR(100),
  path      VARCHAR(160)
);
CREATE TABLE track (
  id        INTEGER    PRIMARY KEY,
  artist_id INTEGER,
  album_id  INTEGER,
  track_no  INTEGER,
  name      VARCHAR(80),
  filename  VARCHAR(80)
);

CREATE INDEX "artist_name" ON "artist" ("name");
CREATE INDEX "album_name"  ON "album"  ("name");
CREATE INDEX "track_name"  ON "track"  ("name");

`
