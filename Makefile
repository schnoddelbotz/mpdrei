
PLATFORMS = linux/amd64 darwin/amd64 windows/amd64 linux/386

VERSION = $(shell git describe --tags | cut -dv -f2)
LDFLAGS := -X main.AppVersion=$(VERSION) -w

BOOTSTRAP = 4.0.0-beta.2

all:
	go get -v
	go install

release: xgo dependencies
	xgo -go 1.9.x -ldflags "$(LDFLAGS)" -targets "$(PLATFORMS)" .

ziprelease: release
	for bin in mpdrei-darwin* mpdrei-linux* mpdrei-windows*; do zip $${bin}_v$(VERSION).zip $$bin; done

xgo:
	go get github.com/karalabe/xgo

dependencies:
	go get -v
	touch dependencies

#
#
### for rebuilding web-app:

assets: webserver/assets.go

webserver/assets.go: go-bindata clean
	mkdir -p mpdrei-web/dist/assets
	cd mpdrei-web; \
	  cp index.html mp3.jpg favicon.ico spinner.gif dist; \
	  cat bootstrap.min.css app.css > dist/assets/app.css; \
	  cat jquery.min.js bootstrap.bundle.min.js app.js > dist/assets/app.js
	cd mpdrei-web/dist/assets; gzip app.css ; gzip app.js
	go-bindata -pkg webserver -prefix "`pwd`/mpdrei-web/dist" -nocompress -nomemcopy -o webserver/assets.go mpdrei-web/dist/...

bootstrap:
	curl -Lso bootstrap.zip \
	  https://github.com/twbs/bootstrap/releases/download/v$(BOOTSTRAP)/bootstrap-$(BOOTSTRAP)-dist.zip
	unzip bootstrap.zip
	cp css/bootstrap.min.css js/bootstrap.bundle.min.js mpdrei-web/
	rm -rf js css bootstrap.zip

go-bindata:
	go get github.com/jteeuwen/go-bindata/...

clean:
	rm -rf mpdrei-linux-* mpdrei-darwin-* mpdrei-windows-* dependencies mpdrei-web/dist
